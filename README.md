## Before usage you need to install deps:

```bash
pip install -r requirements.txt -U
```

## Usage

### Simple client
```bash
python main.py
```

### Data generator
```bash
python vfd_data_generator.py
```
