"""
Example of sending data to API endpoint.

To use our API first we need to get auth token from AWS Cognito.
We need to cache it locally to reuse util it will be expired.

For getting access token from AWS Cognito we need to make HTTPS POST request with the next encoded values:

* Cognito app client ID
* Cognito app client secret.

After we got access token from AWS Cognito we need to cache and use for HTTP POST requests to the API endpoint.
It must be used as a part of 'Authorization' header:

Authorization: Bearer ACCESS_TOKEN.
"""

import json

from datetime import datetime

from utils import make_api_request


# Prod
# APP_CLIENT_ID = '1ngf6ajlv93ppeogq6m7inh88h'
# APP_CLIENT_SECRET = '1ujm0atc5h6tjgnorbt3a274viqigarube3jpublm6bdr14eq13m'
# AUTH_POOL_URL = 'https://gate-pool.auth.us-east-1.amazoncognito.com/oauth2/token'
# API_URL = 'https://5hv19bft8c.execute-api.us-east-1.amazonaws.com/gate-dev/data'

# Dev
APP_CLIENT_ID = '4cdoq1iq6ssdl8ojkjtj9akovh'
APP_CLIENT_SECRET = 'emccn7hopjmp7tcnrvle6tqm5d0dkbhrf49aih8b0nkie5112at'
AUTH_POOL_URL = 'https://dev-gate-pool.auth.us-east-1.amazoncognito.com/oauth2/token'
API_URL = 'https://t9bf2f16i1.execute-api.us-east-1.amazonaws.com/dev-gate/data'

# Should be changed after API endpoint will be ready.
TIMESTAMP = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
API_DATA = json.dumps({
    'site': 'dhl-brescia',
    # List of time-series data points to upload.
    'data': [
        {
            'asset_name': 'Z01MC140',
            'component_type': 'VFD',
            'metrics': [
                {
                    'speed': '1.1',
                    'current': '2',
                    'voltage': '3',
                    'mechanical_power': '5',
                    'timestamp': TIMESTAMP
                },
                {
                    'speed': '6',
                    'current': '7',
                    'voltage': '8',
                    'mechanical_power': '10',
                    'timestamp': TIMESTAMP
                }
            ],
            'temperatures': [
                {
                    'heat_sink_temperature': '9',
                    'timestamp': TIMESTAMP
                },
                {
                    'heat_sink_temperature': '9',
                    'timestamp': TIMESTAMP
                }
            ],
            'lifetimes': [
                {
                    'operating_time': '2',
                    'running_time': '3',
                    'timestamp': TIMESTAMP
                }
            ],
            'statuses': [
                {
                    'status_word': '4',
                    'timestamp': TIMESTAMP
                },
                {
                    'fault': '10',
                    'timestamp': TIMESTAMP
                },
                {
                    'warning': '1.0',
                    'timestamp': TIMESTAMP
                }
            ]
        }
    ]
})


def main():
    """
    Main function.
    """

    make_api_request(API_DATA, API_URL, APP_CLIENT_ID, APP_CLIENT_SECRET, AUTH_POOL_URL)


if __name__ == '__main__':
    main()
