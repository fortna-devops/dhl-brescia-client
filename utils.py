import base64
import json
import os
import time

import requests


AUTH_CACHE_FILE_NAME = './auth_cache.json'


def auth_request(client_id: str, client_secret: str, auth_pool_url: str, scope: str = 'test-resource-service/scope') -> str:
    """
    Makes HTTP POST request to Cognito auth pool and returns response encoded body for further usage.
    """

    encode_data = f'{client_id}:{client_secret}'.encode('utf-8')
    encode_authorization = base64.b64encode(encode_data)
    authorization = encode_authorization.decode('utf-8')

    headers = {
        'Authorization': f'Basic {authorization}'
    }

    data = {
        'grant_type': 'client_credentials',
        'scope': scope
    }

    response = requests.post(url=auth_pool_url, headers=headers, data=data)
    assert response.status_code, 200
    return json.loads(response.content)


def get_access_token(app_client_id, app_client_secret, auth_pool_url) -> str:
    """
    Gets access token from cache and returns it. It refreshes the token if it is already expired.
    """

    # If there is no file with cached access token we simply do request to Cognito and save the response.
    if not os.path.exists(AUTH_CACHE_FILE_NAME):

        auth_data = auth_request(app_client_id, app_client_secret, auth_pool_url)

        with open(AUTH_CACHE_FILE_NAME, 'w') as f:
            json.dump(auth_data, f)

    # If there is no file with cached access token we need to check if this token is not expired.
    # If the token has been expired we refresh it.
    else:

        with open(AUTH_CACHE_FILE_NAME, 'r') as f:
            auth_data = json.load(f)

        mod_time = os.path.getmtime(AUTH_CACHE_FILE_NAME)

        if time.time() - mod_time + 5 > auth_data['expires_in']:

            auth_data = auth_request(app_client_id, app_client_secret, auth_pool_url)

            with open(AUTH_CACHE_FILE_NAME, 'w') as f:
                json.dump(auth_data, f)

    return auth_data['access_token']


def make_api_request(data, url, app_client_id, app_client_secret, auth_pool_url):

    access_token = get_access_token(app_client_id, app_client_secret, auth_pool_url)

    headers = {
        'Authorization': f'Bearer {access_token}'
    }

    response = requests.post(url, headers=headers, data=data)
    print('API response status code:', response.status_code)
    print('API response body:', response.content)

