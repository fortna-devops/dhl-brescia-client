import json
import random
import logging
from datetime import timedelta, datetime
from typing import Tuple, Optional, Dict, List, Any

from utils import make_api_request

logger = logging.getLogger()


SITE: str = 'mhs-emulated'
ASSET_NAME: str = 'SR3-1'


APP_CLIENT_ID: str = 'fckhvq62bsfee8paiemk83jv4'
APP_CLIENT_SECRET: str = '1p1h3mi05bn42gbgq7k9g0upq21f7sjfu08jnlesqb4u9734qr'
AUTH_POOL_URL: str = 'https://gate-pool.auth.us-east-1.amazoncognito.com/oauth2/token'
API_URL: str = 'https://t9bf2f16i1.execute-api.us-east-1.amazonaws.com/dev-gate/data'


STATE_TIMESTAMP_FORMAT: str = '%Y-%m-%d %H:%M:%S.%f'


class StateJsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.strftime(STATE_TIMESTAMP_FORMAT)
        super().default(o)


class StateJsonDecoder(json.JSONDecoder):
    def __init__(self, **kwargs):
        kwargs['object_pairs_hook'] = self._object_pairs_hook
        super().__init__(**kwargs)

    @staticmethod
    def _object_pairs_hook(pairs):
        res = {}
        for key, value in pairs:
            if key.endswith('_timestamp') and value is not None:
                value = datetime.strptime(value, STATE_TIMESTAMP_FORMAT)
            res[key] = value
        return res


class State:

    def __init__(self, asset_status: bool, last_status_change_timestamp: datetime,
                 last_warning_or_fault_timestamp: Optional[datetime] = None,
                 operating_time: int = 0, running_time: int = 0
                 ):
        self.asset_status = asset_status
        self.last_status_change_timestamp = last_status_change_timestamp
        self.last_warning_or_fault_timestamp = last_warning_or_fault_timestamp
        self.operating_time = operating_time
        self.running_time = running_time

    def _to_dict(self):
        return {
            'asset_status': self.asset_status,
            'last_status_change_timestamp': self.last_status_change_timestamp,
            'last_warning_or_fault_timestamp': self.last_warning_or_fault_timestamp,
            'operating_time': self.operating_time,
            'running_time': self.running_time,
        }

    @classmethod
    def load(cls, path):
        try:
            with open(path) as f:
                state = json.load(f, cls=StateJsonDecoder)
                return cls(**state)
        except:
            logger.warning(f'Can\'t load state from {path}')

        return None

    def save(self, path):
        with open(path, 'w') as f:
            json.dump(self._to_dict(), f, cls=StateJsonEncoder, indent=4)


class DataGenerator:
    gen_period: int = 60  # seconds
    state_path: str = 'generator_state.json'
    timestamp_format: str = '%Y-%m-%d %H:%M:%S.%f'

    data_source_type: str = 'vfd'
    motor_component_type: str = 'MOTOR'
    vfd_component_type: str = 'VFD'

    asset_off_period: timedelta = timedelta(hours=random.uniform(1., 2.))
    asset_on_period: timedelta = timedelta(days=1) - asset_off_period

    metrics_range_map: Dict[str, Tuple[float, float]] = {
        'speed': (1., 128.),
        'current': (.1, 1.5),
        'voltage': (1., 40.),
        'mechanical_power': (.1, 1.),
        'cos_phi': (0., 1.),
    }

    temperature_range: Tuple[float, float] = (18., 22.)

    warning_or_fault_period: timedelta = timedelta(days=random.uniform(3., 4.))
    warning_code: str = '12.1'
    fault_code: str = '15.2'

    def __init__(self, site: str, asset_name: str):
        self._timestamp = datetime.utcnow()
        self._state = self._load_state()

        self._site = site
        self._asset_name = asset_name

    def _load_state(self) -> State:
        state = State.load(self.state_path)
        if state is None:
            state = State(asset_status=True, last_status_change_timestamp=self._timestamp)
        return state

    def _save_state(self):
        self._state.save(self.state_path)

    @staticmethod
    def _value_to_str(value: float) -> str:
        return str(round(value, 1))

    @staticmethod
    def _random_uniform(a: float, b: float, mod: float) -> float:
        return a + (b-a) * mod

    def _get_metric_value(self, gen_range: Tuple[float, float], mod: float = random.random()) -> str:
        if not self._state.asset_status:
            value = 0.
        else:
            value = self._random_uniform(*gen_range, mod)
        return self._value_to_str(value)

    def _get_metrics_data(self) -> List[Dict[str, str]]:
        metrics = []
        for second in range(self.gen_period):
            timestamp = self._timestamp + timedelta(seconds=second)
            row = {
                'timestamp': timestamp.strftime(self.timestamp_format)
            }

            mod = random.random()
            for metric_name, gen_range in self.metrics_range_map.items():
                row[metric_name] = self._get_metric_value(gen_range, mod)

            metrics.append(row)

        return metrics

    def _get_temperature_data(self) -> List[Dict[str, str]]:
        return [{
            'heat_sink_temperature': self._value_to_str(random.uniform(*self.temperature_range)),
            'timestamp': self._timestamp.strftime(self.timestamp_format)
        }]

    def _get_lifetime_data(self) -> List[Dict[str, str]]:
        if self._timestamp.minute != 0:
            return []

        return [{
            'operating_time': self._value_to_str(self._state.operating_time / 3600),
            'running_time': self._value_to_str(self._state.running_time / 3600),
            'timestamp': self._timestamp.strftime(self.timestamp_format)
        }]

    def _get_warning_data_row(self) -> Dict[str, str]:
        return {
            'warning': self.warning_code,
            'timestamp': self._timestamp.strftime(self.timestamp_format)
        }

    def _get_fault_data_row(self) -> Dict[str, str]:
        return {
            'fault': self.fault_code,
            'timestamp': self._timestamp.strftime(self.timestamp_format)
        }

    def _get_statuses_data(self) -> List[Dict[str, str]]:
        statuses = []

        if self._state.last_warning_or_fault_timestamp is None or \
                self._timestamp - self._state.last_warning_or_fault_timestamp >= self.warning_or_fault_period:
            get_warning_or_fault = random.choice((self._get_warning_data_row, self._get_fault_data_row))
            statuses.append(get_warning_or_fault())
            self._state.last_warning_or_fault_timestamp = self._timestamp

        return statuses

    def _update_state(self):
        # Update asset status
        check_period = self.asset_on_period if self._state.asset_status else self.asset_off_period
        if self._timestamp - self._state.last_status_change_timestamp >= check_period:
            # it's time to change asset status
            self._state.asset_status = not self._state.asset_status
            self._state.last_status_change_timestamp = self._timestamp

        # Update operating_time and running_time
        if self._state.asset_status:
            self._state.running_time += self.gen_period
        self._state.operating_time += self.gen_period

    def generate(self) -> Dict[str, Any]:
        self._update_state()

        data = {
            'site': self._site,
            'data': [
                {
                    'asset_name': self._asset_name,
                    'component_type': self.motor_component_type,
                    'data_source_type': self.data_source_type,
                    'metrics': self._get_metrics_data(),
                },
                {
                    'asset_name': self._asset_name,
                    'component_type': self.vfd_component_type,
                    'data_source_type': self.data_source_type,
                    'temperatures': self._get_temperature_data(),
                    'lifetimes': self._get_lifetime_data(),
                    'statuses': self._get_statuses_data(),
                }
            ]
        }

        self._save_state()
        return data


def main():
    gen = DataGenerator(site=SITE, asset_name=ASSET_NAME)
    data = gen.generate()
    data = json.dumps(data)
    make_api_request(data, API_URL, APP_CLIENT_ID, APP_CLIENT_SECRET, AUTH_POOL_URL)


if __name__ == '__main__':
    main()
